package application;

import java.rmi.Remote;
import java.rmi.RemoteException;
 
public interface StackInterface extends Remote {
    public void push(String x) throws RemoteException;
    public String pop() throws RemoteException;
}
