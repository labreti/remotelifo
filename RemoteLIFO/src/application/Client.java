package application;

import java.rmi.Naming;

public class Client {
	public static void main(String[] args)
			throws Exception {
		String server=args[0];
		String url="rmi://"+server+":1111/RemoteStack";
		StackInterface name = (StackInterface) Naming.lookup(url);
		if ( args.length < 2 ) {
			System.out.println(name.pop());
		} else {
			name.push(args[1]);
		}
	}	
}
