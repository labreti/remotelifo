package application;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Stack;

public class Server extends UnicastRemoteObject implements StackInterface {
	private static final long serialVersionUID = 1L;
	private Stack<String> stack = new Stack<String>();
	public synchronized void push(String item) throws RemoteException {
		stack.push(item);
		return;
	}
	public synchronized String pop() throws RemoteException {
		String item=stack.pop();
		return item ;
	}
	public Server() throws RemoteException {}
	public static void main(String[] args) throws RemoteException {
        System.setProperty("java.rmi.server.hostname", "192.168.5.1");
		Registry reg = LocateRegistry.createRegistry(1111);
		reg.rebind("RemoteStack", new Server());
		System.out.println("Server Ready");
	}
}
